//
//  FDParkingLotPlaceItemTest.swift
//  fdmaps
//
//  Created by 吳天元 on 5/11/16.
//  Copyright © 2016 吳天元. All rights reserved.
//

import XCTest
@testable import FDMap

class FDParkingLotPlaceItemTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let manager = FDPlacesManager.shareInstance
        let bundle = NSBundle(forClass: FDParkingLotPlaceItemTest.self)
        guard let path = bundle.pathForResource("test_places", ofType: "geojson") else { return }
        if NSFileManager.defaultManager().fileExistsAtPath(path) { manager.loadPlaces(path) }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_parsingJson_01() {
        let places = FDPlacesManager.shareInstance.places
        if places.isEmpty {
            XCTAssert(false)
            return
        }
        guard let place = places[1] as? FDParkingLotPlaceItem else {
            XCTAssert(false)
            return
        }
        
        XCTAssertEqual(place.start, 25200)
        XCTAssertEqual(place.end, 82800)
    }
    
    func test_parsingJson_02() {
        let places = FDPlacesManager.shareInstance.places
        if places.count < 2 {
            XCTAssert(false)
            return
        }
        guard let place = places[2] as? FDParkingLotPlaceItem else {
            XCTAssert(false)
            return
        }
        
        XCTAssertEqual(place.start, 28800)
        XCTAssertEqual(place.end, 72000)
    }
    
    func test_parsingJson_06() {
        let places = FDPlacesManager.shareInstance.places
        if places.count < 5 {
            XCTAssert(false)
            return
        }
        let place = places[10000001] as? FDParkingLotPlaceItem
        XCTAssertNil(place)
    }
    
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
