//
//  FDGMSearchHistoryTest.swift
//  fdmaps
//
//  Created by 吳天元 on 5/11/16.
//  Copyright © 2016 吳天元. All rights reserved.
//

import XCTest
@testable import FDMap

class FDGMSearchHistoryTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func test_saveAndLoad() {
        let map = FDGoogleMap()
        map.searchResults.removeAll()
        map.searchResults.append(FDGMSearchItem(title: "title 01", placeId: "place id 01"))
        map.searchResults.append(FDGMSearchItem(title: "title 02", placeId: "place id 02"))
        map.searchResults.append(FDGMSearchItem(title: "title 03", placeId: "place id 03"))
        map.searchResults.append(FDGMSearchItem(title: "title 04", placeId: "place id 04"))
        map.searchResults.append(FDGMSearchItem(title: "title 05", placeId: "place id 05"))
        FDGMSearchHistory.save(map)
        
        map.searchResults.removeAll()
        FDGMSearchHistory.load(map)
        XCTAssertEqual(map.searchResults.count, 5)
        XCTAssertEqual(map.searchResults[0].title, "title 01")
        XCTAssertEqual(map.searchResults[0].placeId, "place id 01")
        XCTAssertEqual(map.searchResults[1].title, "title 02")
        XCTAssertEqual(map.searchResults[1].placeId, "place id 02")
        XCTAssertEqual(map.searchResults[2].title, "title 03")
        XCTAssertEqual(map.searchResults[2].placeId, "place id 03")
        XCTAssertEqual(map.searchResults[3].title, "title 04")
        XCTAssertEqual(map.searchResults[3].placeId, "place id 04")
        XCTAssertEqual(map.searchResults[4].title, "title 05")
        XCTAssertEqual(map.searchResults[4].placeId, "place id 05")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}

