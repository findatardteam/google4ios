//
//  FDGMSearchTableDelegeteTests.swift
//  fdmaps
//
//  Created by 吳天元 on 5/12/16.
//  Copyright © 2016 吳天元. All rights reserved.
//

import XCTest
@testable import FDMap

class FDGMSearchTableDelegeteTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func test_numberOfRowsInSection() {
        let map = FDGoogleMap()
        map.searchResults.removeAll()
        map.searchResults.append(FDGMSearchItem(title: "title 01", placeId: "place id 01"))
        map.searchResults.append(FDGMSearchItem(title: "title 02", placeId: "place id 02"))
        map.searchResults.append(FDGMSearchItem(title: "title 03", placeId: "place id 03"))
        map.searchResults.append(FDGMSearchItem(title: "title 04", placeId: "place id 04"))
        map.searchResults.append(FDGMSearchItem(title: "title 05", placeId: "place id 05"))
        
        XCTAssertEqual(map.searchResults.count, map.tableDelegate.tableView(map.resultsTable, numberOfRowsInSection: 0))
    }
    
    func test_didSelectRowAtIndexPath() {
        let map = FDGoogleMap()
        map.searchResults.removeAll()
        map.searchResults.append(FDGMSearchItem(title: "title 01", placeId: "place id 01"))
        map.searchResults.append(FDGMSearchItem(title: "title 02", placeId: "place id 02"))
        map.searchResults.append(FDGMSearchItem(title: "title 03", placeId: "place id 03"))
        map.searchResults.append(FDGMSearchItem(title: "title 04", placeId: "place id 04"))
        map.searchResults.append(FDGMSearchItem(title: "title 05", placeId: "place id 05"))

        var cell = map.tableDelegate.tableView(map.resultsTable, cellForRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 0))
        XCTAssertEqual(cell.textLabel?.text, "title 01")
        cell = map.tableDelegate.tableView(map.resultsTable, cellForRowAtIndexPath: NSIndexPath(forRow: 1, inSection: 0))
        XCTAssertEqual(cell.textLabel?.text, "title 02")
        cell = map.tableDelegate.tableView(map.resultsTable, cellForRowAtIndexPath: NSIndexPath(forRow: 2, inSection: 0))
        XCTAssertEqual(cell.textLabel?.text, "title 03")
        cell = map.tableDelegate.tableView(map.resultsTable, cellForRowAtIndexPath: NSIndexPath(forRow: 3, inSection: 0))
        XCTAssertEqual(cell.textLabel?.text, "title 04")
        cell = map.tableDelegate.tableView(map.resultsTable, cellForRowAtIndexPath: NSIndexPath(forRow: 4, inSection: 0))
        XCTAssertEqual(cell.textLabel?.text, "title 05")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}