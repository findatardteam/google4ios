//
//  FDGMPlacesListTests.swift
//  FDMap
//
//  Created by 吳天元 on 5/17/16.
//  Copyright © 2016 FinData. All rights reserved.
//

//
//  FDMapTests.swift
//  FDMapTests
//
//  Created by 吳天元 on 5/13/16.
//  Copyright © 2016 吳天元. All rights reserved.
//

import XCTest
import MapKit
@testable import FDMap

class FDGMPlacesListTests: XCTestCase {
    let location = CLLocationCoordinate2D(latitude: 25.05924267, longitude: 121.57703872)
    
    var places = Array<FDPlaceItem>()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let manager = FDPlacesManager.shareInstance
        let bundle = NSBundle(forClass: FDGMPlacesListTests.self)
        guard let path = bundle.pathForResource("test_places", ofType: "geojson") else { return }
        if NSFileManager.defaultManager().fileExistsAtPath(path) { manager.loadPlaces(path) }
        
        places.removeAll()
        for (_, place) in manager.places { places.append(place) }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func test_updateListByNoFilterSortByDistance() {
        let list = FDGMPlacesList()
        let results = list.sortAndFilter(self.location, places: self.places)
        
        XCTAssertEqual(results.count, 6)
        if results.count != 6 { return }
        XCTAssertEqual(results[0].name, "大潤發一館")
        XCTAssertEqual(results[1].name, "大潤發二館")
        XCTAssertEqual(results[2].name, "B&Q特力屋")
        XCTAssertEqual(results[3].name, "好市多")
        XCTAssertEqual(results[4].name, "燦坤")
        XCTAssertEqual(results[5].name, "台北科技大學")
    }
    
    func test_updateListByNoFilterSortBySpace() {
        let list = FDGMPlacesList()
        list.searchBar.selectedScopeButtonIndex = 1
        let results = list.sortAndFilter(self.location, places: self.places)
        
        XCTAssertEqual(results.count, 6)
        if results.count != 6 { return }
        XCTAssertEqual(results[0].name, "B&Q特力屋")
        XCTAssertEqual(results[1].name, "好市多")
        XCTAssertEqual(results[2].name, "大潤發二館")
        XCTAssertEqual(results[3].name, "燦坤")
        XCTAssertEqual(results[4].name, "大潤發一館")
        XCTAssertEqual(results[5].name, "台北科技大學")
    }
    
    func test_updateListByNoFilterSortByPrice() {
        let list = FDGMPlacesList()
        list.searchBar.selectedScopeButtonIndex = 2
        let results = list.sortAndFilter(self.location, places: self.places)
        
        XCTAssertEqual(results.count, 6)
        if results.count != 6 { return }
        XCTAssertEqual(results[0].name, "B&Q特力屋")
        XCTAssertEqual(results[1].name, "好市多")
        XCTAssertEqual(results[2].name, "大潤發一館")
        XCTAssertEqual(results[3].name, "大潤發二館")
        XCTAssertEqual(results[4].name, "燦坤")
        XCTAssertEqual(results[5].name, "台北科技大學")
    }
    
    func test_updateListByFilterSortByDistance() {
        let list = FDGMPlacesList()
        list.searchBar.text = "大"
        let results = list.sortAndFilter(self.location, places: self.places)
        
        XCTAssertEqual(results.count, 3)
        if results.count != 3 { return }
        XCTAssertEqual(results[0].name, "大潤發一館")
        XCTAssertEqual(results[1].name, "大潤發二館")
        XCTAssertEqual(results[2].name, "台北科技大學")
        
    }
    
    func test_updateListByFilterSortBySpace() {
        let list = FDGMPlacesList()
        list.searchBar.text = "大"
        list.searchBar.selectedScopeButtonIndex = 1
        let results = list.sortAndFilter(self.location, places: self.places)
        
        XCTAssertEqual(results.count, 3)
        if results.count != 3 { return }
        XCTAssertEqual(results[0].name, "大潤發二館")
        XCTAssertEqual(results[1].name, "大潤發一館")
        XCTAssertEqual(results[2].name, "台北科技大學")
    }
    
    func test_updateListByFilterSortByPrice() {
        let list = FDGMPlacesList()
        list.searchBar.text = "大"
        list.searchBar.selectedScopeButtonIndex = 2
        let results = list.sortAndFilter(self.location, places: self.places)
        
        XCTAssertEqual(results.count, 3)
        if results.count != 3 { return }
        
        XCTAssertEqual(results[0].name, "大潤發一館")
        XCTAssertEqual(results[1].name, "大潤發二館")
        XCTAssertEqual(results[2].name, "台北科技大學")
    }
    
}

