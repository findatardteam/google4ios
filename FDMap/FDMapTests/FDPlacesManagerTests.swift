//
//  File.swift
//  fdmaps
//
//  Created by 吳天元 on 5/11/16.
//  Copyright © 2016 吳天元. All rights reserved.
//

import XCTest
import MapKit
@testable import FDMap

class FDPlacesManagerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let manager = FDPlacesManager.shareInstance
        let bundle = NSBundle(forClass: FDPlacesManagerTests.self)
        guard let path = bundle.pathForResource("test_places", ofType: "geojson") else { return }
        if NSFileManager.defaultManager().fileExistsAtPath(path) { manager.loadPlaces(path) }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func test_loadPlaces() {
        let manager = FDPlacesManager.shareInstance
        if FDPlacesManager.shareInstance.places.count != 6 {
            XCTAssertEqual(FDPlacesManager.shareInstance.places.count, 6)
            return
        }
        XCTAssertEqual(manager.places[1]!.name, "大潤發一館")
        XCTAssertEqual(manager.places[2]!.name, "大潤發二館")
        XCTAssertEqual(manager.places[3]!.name, "B&Q特力屋")
        XCTAssertEqual(manager.places[4]!.name, "燦坤")
        XCTAssertEqual(manager.places[5]!.name, "好市多")
        XCTAssertEqual(manager.places[10000001]!.name, "台北科技大學")
    }
    
    func test_placesInRegion() {
        let nearLeft = CLLocationCoordinate2D(latitude: 25.06, longitude: 121.57)
        let farRight = CLLocationCoordinate2D(latitude: 25.07, longitude: 121.58)
        let places = FDPlacesManager.shareInstance.placesInRegion(nearLeft, farRight: farRight)
        if places.count != 5 {
            XCTAssertEqual(places.count, 5)
            return
        }
        XCTAssertEqual(places[1]!.name, "大潤發一館")
        XCTAssertEqual(places[2]!.name, "大潤發二館")
        XCTAssertEqual(places[3]!.name, "B&Q特力屋")
        XCTAssertEqual(places[4]!.name, "燦坤")
        XCTAssertEqual(places[5]!.name, "好市多")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
