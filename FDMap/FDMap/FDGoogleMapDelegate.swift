//
//  FDGoogleMapDelegate.swift
//  fdmaps
//
//  Created by 吳天元 on 2016/5/9.
//  Copyright © 2016年 吳天元. All rights reserved.
//

import Foundation
import GoogleMaps
import SwiftyJSON

class FDGoogleMapDelegate:NSObject, GMSMapViewDelegate {
    struct Region {
        let nearLeft: CLLocationCoordinate2D
        let farRight: CLLocationCoordinate2D
    }
    
    let map: FDGoogleMap
    var region: Region?
    var infoWindow: FDGMInfoWindow?
    var text: String?
    
    init(map: FDGoogleMap) {
        self.map = map
    }
    
    func mapView(mapView: GMSMapView, didChangeCameraPosition position: GMSCameraPosition) {
        if position.zoom < 15 {
            self.clearMarkers()
            self.region = nil
            return
        }
        
        if self.map.markers.count > 2 { return }
        
        self.clearMarkers()
        let items = FDPlacesManager.shareInstance.places
        for (_, item) in items {
            self.map.createMarker(item)
        }
    }
    
    func clearMarkers() {
        let data = self.map.markers[0]?.userData as? FDPlaceItem
        self.map.markers.removeAll()
        self.map.mapView.clear()
        
        if data != nil { self.map.createMarker(data!) }
    }
    
    func containsRegion(visible: GMSVisibleRegion) -> Bool {
        guard let region = self.region else { return false }
        
        if region.nearLeft.latitude > visible.nearLeft.latitude { return false }
        if region.farRight.latitude < visible.farRight.latitude { return false }
        if region.nearLeft.longitude > visible.nearLeft.longitude { return false }
        if region.farRight.longitude < visible.farRight.longitude { return false }
        return true
    }
    
    func generateRegion(visible: GMSVisibleRegion) -> Void {
        let left = visible.nearLeft.latitude
        let right = visible.farRight.latitude
        let far = visible.nearLeft.longitude
        let near = visible.farRight.longitude
        
        let centerX = (left + right) / 2.0
        let centerY = (far + near) / 2.0
        let dx = abs((left - right) * 1.5)
        let dy = abs((far - near) * 1.5)
        
        let nearLeft = CLLocationCoordinate2D(latitude: centerX - dx, longitude: centerY - dy)
        let farRight = CLLocationCoordinate2D(latitude: centerX + dx, longitude: centerY + dy)
        self.region = Region(nearLeft: nearLeft, farRight: farRight)
    }
    
    func mapView(mapView: GMSMapView, didTapMarker marker: GMSMarker) -> Bool {
        return false
    }
    
    func mapView(mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        guard let item = marker.userData as? FDPlaceItem else { return nil }
        if self.infoWindow?.placeItem === item { return self.infoWindow }
        self.infoWindow = FDGMInfoWindowFactory.createInfoWindow(mapView, item: item)
        return self.infoWindow
    }
    
    func mapView(mapView: GMSMapView, didTapInfoWindowOfMarker marker: GMSMarker) {
        let question = UIAlertController(title: "導航", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let dest = String(marker.position.latitude) + "," + String(marker.position.longitude)
        
        if UIApplication.sharedApplication().canOpenURL(NSURL(string: "comgooglemaps://")!) {
            let callaction = UIAlertAction(title: "Google Map 導航",style: UIAlertActionStyle.Default, handler:{
                (action:UIAlertAction!) -> Void in
                let url = "comgooglemaps://?daddr=" + dest + "&directionsmode=driving"
                UIApplication.sharedApplication().openURL(NSURL(string: url)!)
            });
            question.addAction(callaction)
        }
        
        let callaction = UIAlertAction(title: "Apple Map 導航",style: UIAlertActionStyle.Default, handler:{
            (action:UIAlertAction!) -> Void in
            let url = "http://maps.apple.com/?daddr=" + dest + "&dirflg=d"
            UIApplication.sharedApplication().openURL(NSURL(string: url)!)
        });
        question.addAction(callaction)
        question.addAction(UIAlertAction(title: "取消", style: UIAlertActionStyle.Cancel, handler: nil))
        
        self.map.viewController.presentViewController(question, animated: true, completion: nil)
    }
}
