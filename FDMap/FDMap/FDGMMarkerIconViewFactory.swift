//
//  FDGMMarkerIconViewFactory.swift
//  fdmaps
//
//  Created by 吳天元 on 5/10/16.
//  Copyright © 2016 吳天元. All rights reserved.
//

import UIKit

class FDGMMarkerIconViewFactory {
    static func createIconView(item: FDPlaceItem) -> UIView {
        switch item.type {
        case FDPlaceItemType.ParkingLot:
            let view = FDGMParkingLotMarkerIconView()
            guard let placeItem = item as? FDParkingLotPlaceItem else { return UIImageView(image: UIImage(named: "placeholder-red")) }
            view.placeItem = placeItem
            return view
            
        default:
            return UIImageView(image: UIImage(named: "placeholder-red"))
        }
    }
}