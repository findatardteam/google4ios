//
//  FDGMInfoWindowFactory.swift
//  fdmaps
//
//  Created by 吳天元 on 5/11/16.
//  Copyright © 2016 吳天元. All rights reserved.
//

import GoogleMaps
import UIKit

class FDGMInfoWindowFactory {
    static func createInfoWindow(mapView: GMSMapView, item: FDPlaceItem) -> FDGMInfoWindow {
        switch item.type {
        case FDPlaceItemType.ParkingLot:
            guard let _ = item as? FDParkingLotPlaceItem else { return FDGMInfoWindowFactory.createDefaultInfoWindow(mapView, item: item) }
            
            let window = FDGMParkingLotInfoWindow()
            FDGMInfoWindowFactory.initInfoWindow(window, mapView: mapView, item: item)
            return window
            
        case FDPlaceItemType.Unknown:
            return FDGMInfoWindowFactory.createDefaultInfoWindow(mapView, item: item)
        }
    }
    
    static func createDefaultInfoWindow(mapView: GMSMapView, item: FDPlaceItem) -> FDGMInfoWindow {
        let window = FDGMInfoWindow()
        FDGMInfoWindowFactory.initInfoWindow(window, mapView: mapView, item: item)
        return window
    }
    
    static func initInfoWindow(window: FDGMInfoWindow, mapView: GMSMapView, item: FDPlaceItem) {
        window.placeItem = item
        window.mapView = mapView
        if mapView.myLocation != nil { window.initDistanceAndDuration(mapView.myLocation!.coordinate) }
    }
}