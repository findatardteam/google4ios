//
//  FDGoogleMapSearchBarDeleget.swift
//  fdmaps
//
//  Created by 吳天元 on 2016/5/9.
//  Copyright © 2016年 吳天元. All rights reserved.
//

import Foundation
import GoogleMaps

class FDGMSearchBarDelegete: NSObject, UISearchBarDelegate {
    let TAIWAN_REGION = GMSCoordinateBounds(coordinate: CLLocationCoordinate2D(latitude: 21.749295836732088, longitude: 119.66308593749999), coordinate: CLLocationCoordinate2D(latitude: 25.611809521055477, longitude: 122.135009765625))
    let map: FDGoogleMap;
    
    init(map: FDGoogleMap) {
        self.map = map
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        self.map.addSubview(self.map.resultsTable)
        
        if searchBar.text == nil {
            self.searchBar(searchBar, textDidChange: "")
        }
        else {
            self.searchBar(searchBar, textDidChange: searchBar.text!)
        }
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        
        self.map.resultsTable.removeFromSuperview()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.text = nil
        self.searchBarTextDidEndEditing(searchBar)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.map.searchResults.removeAll()
            self.map.loadHistory()
            self.map.resultsTable.reloadData()
            return
        }
        
        self.map.placesClient.autocompleteQuery(searchText, bounds: self.TAIWAN_REGION, filter: nil, callback: {
            (results: Array<GMSAutocompletePrediction>?, error: NSError?) -> Void in
            if let error = error {
                print("\(#file) \(#function) \(#line) \(error)")
                return
            }
            
            self.map.searchResults.removeAll()
            guard let results = results else { return }
            
            for obj in results {
                let item = FDGMSearchItem(title: obj.attributedFullText.string, placeId: obj.placeID!)
                self.map.searchResults.append(item)
            }
            self.map.resultsTable.reloadData()
        })
    }
}