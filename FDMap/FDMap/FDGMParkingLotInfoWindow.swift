//
//  FDGMParkingLotInfoWindow.swift
//  fdmaps
//
//  Created by 吳天元 on 5/11/16.
//  Copyright © 2016 吳天元. All rights reserved.
//

import UIKit
import MapKit
import SwiftyJSON

class FDGMParkingLotInfoWindow: FDGMInfoWindow {
    var businessTime: String!
    
    override func initView() -> Void {
        super.initView()
        guard let placeItem = self.placeItem as? FDParkingLotPlaceItem else { return }
        
        guard let start = placeItem.start else { return }
        guard let end = placeItem.end else { return }
        var now = NSDate().timeIntervalSince1970
        now = now - (floor(now / 86400) * 86400)
        let timezone = NSTimeZone.localTimeZone()
        now += NSTimeInterval(timezone.secondsFromGMT)
        
        self.businessTime = (now > start && now < end) ? "營業中 " : "休息中 "
        let startHour = Int(start / 3600)
        let startMin = Int(start/60) - (startHour * 60)
        let endHour = Int(end / 3600)
        let endMin = Int(end/60) - (endHour * 60)
        self.businessTime! += String(format: "%02d:%02d - %02d:%02d", startHour, startMin, endHour, endMin)
        
        var text = self.businessTime!
        text += "\n距離：-"
        text += "\n時間：-"
        self.content.text = text
    }
    
    override func updateDistanceAndDurationString(distance: String, duration: String) {
        var text = self.businessTime!
        text += "\n距離：" + distance
        text += "\n時間：" + duration
        self.content.text = text
    }
}
