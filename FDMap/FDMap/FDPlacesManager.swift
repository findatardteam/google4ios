//
//  FDPlaceManager.swift
//  fdmaps
//
//  Created by 吳天元 on 2016/5/10.
//  Copyright © 2016年 吳天元. All rights reserved.
//

import Foundation
import GoogleMaps
import MapKit
import SwiftyJSON

class FDPlacesManager {
    static let shareInstance = FDPlacesManager()
    var places = Dictionary<Int, FDPlaceItem>()
    
    private init() {
        let path = NSBundle.mainBundle().pathForResource("Places", ofType: "geojson")
        self.loadPlaces(path!)
    }
    
    func loadPlaces(path: String) -> Void {
        self.places.removeAll()
        guard let data = NSData(contentsOfFile: path) else { return }
        let json = JSON(data: data)
        
        for (_,subJson):(String, JSON) in json["features"] {
            let property = subJson["properties"]
            guard let latitude = subJson["geometry"]["coordinates"][1].double else { continue }
            guard let longitude = subJson["geometry"]["coordinates"][0].double else { continue }
            
            let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            guard let place = FDPlaceItemFactory.createItem(location, property: property) else { continue }
            self.places[place.id] = place
        }
    }
    
    func placesInRegion(nearLeft: CLLocationCoordinate2D, farRight: CLLocationCoordinate2D) -> Dictionary<Int,FDPlaceItem> {
        var dictionary = Dictionary<Int, FDPlaceItem>()
        for (key, place) in self.places {
            if nearLeft.latitude > place.location.latitude { continue }
            if farRight.latitude < place.location.latitude { continue }
            if nearLeft.longitude > place.location.longitude { continue }
            if farRight.longitude < place.location.longitude { continue }
            
            dictionary[key] = place
        }
        return dictionary
    }
}