//
//  FDPlaceListSearchBarDelegete.swift
//  FDMap
//
//  Created by 吳天元 on 5/13/16.
//  Copyright © 2016 FinData. All rights reserved.
//

import UIKit

class FDGMPlaceListSearchBarDelegete: NSObject, UISearchBarDelegate {
    let list: FDGMPlacesList;
    
    init(list: FDGMPlacesList) {
        self.list = list
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        self.list.updateList()
    }
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        self.list.updateList()
    }
}