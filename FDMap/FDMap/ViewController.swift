//
//  ViewController.swift
//  fdmaps
//
//  Created by 吳天元 on 2016/5/9.
//  Copyright © 2016年 吳天元. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var map: FDGoogleMap!
    var placesList: FDGMPlacesList!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.map = FDGoogleMap(frame: self.view.frame)
        self.map.viewController = self
        self.view.insertSubview(self.map, atIndex: 0)
        
        self.placesList = FDGMPlacesList(frame: self.view.frame)
        self.placesList.mapView = self.map.mapView
        self.placesList.viewController = self
        
        let button = UIButton()
        button.frame = CGRectMake(self.view.frame.size.width - 40, 64, 32, 32)
        button.setImage(UIImage(named: "switch_map_list"), forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(self.onListAndMapSwitchButtonClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(button)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        super.viewWillAppear(animated)
    }
    
    func onListAndMapSwitchButtonClicked(button: UIButton) {
        if self.view.subviews.contains(self.map) {
            self.map.removeFromSuperview()
            self.placesList.updateList(self.map.mapView.myLocation?.coordinate)
            self.view.insertSubview(self.placesList, atIndex: 0)
        }
        else if self.view.subviews.contains(self.placesList) {
            self.placesList.removeFromSuperview()
            self.view.insertSubview(self.map, atIndex: 0)
        }
        else {
            
        }
    }
}

