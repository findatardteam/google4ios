//
//  FDGMSearchItem.swift
//  fdmaps
//
//  Created by 吳天元 on 2016/5/9.
//  Copyright © 2016年 吳天元. All rights reserved.
//

import Foundation

struct FDGMSearchItem {
    var title: String
    var placeId: String
}