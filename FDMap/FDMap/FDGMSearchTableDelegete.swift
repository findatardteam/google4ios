//
//  FDGMSearchTableDelegete.swift
//  fdmaps
//
//  Created by 吳天元 on 2016/5/9.
//  Copyright © 2016年 吳天元. All rights reserved.
//

import GoogleMaps
import SwiftyJSON
import UIKit

class FDGMSearchTableDelegete: NSObject, UITableViewDataSource, UITableViewDelegate {
    let map: FDGoogleMap
    
    init(map: FDGoogleMap) {
        self.map = map
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.map.searchResults.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "MapSearchTableCellIdentifier"
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        if cell == nil { cell = UITableViewCell() }
        
        let item = self.map.searchResults[indexPath.row]
        cell?.textLabel?.text = item.title
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        let item = self.map.searchResults[indexPath.row]
        self.map.searchBar.text = item.title
        
        self.map.placesClient.lookUpPlaceID(item.placeId, callback: { (place: GMSPlace?, error: NSError?) -> Void in
            if let error = error {
                print("\(#file) \(#function) \(#line) \(error.localizedDescription)")
                return
            }
            
            guard let place = place else { return }
            tableView.removeFromSuperview()
            self.map.searchResults.removeAll()
            let marker = self.map.markers[0]
            if marker != nil {
                marker?.map = nil
                self.map.markers.removeValueForKey(0)
            }
            self.map.searchDelegete.searchBarTextDidEndEditing(self.map.searchBar)
            
            tableView.reloadData()
            let camera = GMSCameraPosition.cameraWithLatitude(place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 15)
            self.map.mapView.camera = camera
            let placeItem = FDPlaceItem(id: 0, name: place.name, location: place.coordinate, type: FDPlaceItemType.Unknown)
            self.map.createMarker(placeItem)
            self.map.addHistory(item)
        })
    }
}