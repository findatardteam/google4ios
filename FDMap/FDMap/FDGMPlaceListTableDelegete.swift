//
//  FDPlaceListSearchTableDelegete.swift
//  FDMap
//
//  Created by 吳天元 on 5/13/16.
//  Copyright © 2016 FinData. All rights reserved.
//

import UIKit
import GoogleMaps

class FDGMPlaceListTableDelegete: NSObject, UITableViewDataSource, UITableViewDelegate {
    let list: FDGMPlacesList
    
    init(list: FDGMPlacesList) {
        self.list = list
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.list.results.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let item = self.list.results[indexPath.row]
        
        
        let cellIdentifier = "PlaceListTableCellIdentifier"
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
            let mapView = GMSMapView()
            mapView.frame = CGRectMake(tableView.frame.size.width - 150, 5, 140, 140)
            mapView.settings.setAllGesturesEnabled(false)
            cell?.contentView.addSubview(mapView)
            self.updateMapView(mapView, item: item)
        }
        else {
            for subView in (cell?.contentView.subviews)! {
                guard let mapView = subView as? GMSMapView else { continue }
                
                mapView.clear()
                self.updateMapView(mapView, item: item)
                break
            }
        }
        
        cell?.textLabel?.text = item.name
        guard let parkingItem = item as? FDParkingLotPlaceItem else { return cell! }
        cell?.textLabel?.text = String(format: "%@ 空位：%d 價格：%d", parkingItem.name, parkingItem.emptySpace, parkingItem.price)
        return cell!
    }
    
    func updateMapView(mapView: GMSMapView, item: FDPlaceItem) {
        let marker = GMSMarker(position: item.location)
        marker.title = item.name
        marker.map = mapView
        
        let camera = GMSCameraPosition.cameraWithLatitude(item.location.latitude, longitude: item.location.longitude, zoom: 15)
        mapView.camera = camera
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.list.searchBar.resignFirstResponder()
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        let item = self.list.results[indexPath.row]
        let viewController =  FDGMPlaceViewController()
        viewController.placeItem = item
        self.list.viewController.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 150.0
    }
}