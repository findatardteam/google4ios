//
//  BaseFDMap.swift
//  fdmaps
//
//  Created by 吳天元 on 2016/5/9.
//  Copyright © 2016年 吳天元. All rights reserved.
//

import UIKit

class BaseFDMap: UIView {
    var searchBar: UISearchBar!
    let resultsTable = UITableView()
    
    init() {
        super.init(frame: CGRect.zero)
        self.initSearchBar()
        self.initSearchTableView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initSearchBar()
        self.initSearchTableView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initSearchBar()
        self.initSearchTableView()
    }
    
    func initSearchBar() -> Void {
        self.searchBar = UISearchBar()
        self.searchBar.frame = CGRectMake(0, 0, self.frame.width, 35)
        self.searchBar.placeholder = "Search here..."
        self.searchBar.sizeToFit()
        self.addSubview(self.searchBar)
    }
    
    func initSearchTableView() -> Void {
        guard let searchBar = self.searchBar else { return }
        
        let y = searchBar.frame.height
        var frame = self.frame
        frame.origin.y = y
        frame.size.height -= y
        self.resultsTable.frame = frame
    }
    
    override var frame: CGRect {
        didSet {
            self.initSearchBar()
            self.initSearchTableView()
        }
    }
    
}
