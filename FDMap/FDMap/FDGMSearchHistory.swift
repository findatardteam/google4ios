//
//  FDGMSearchHistory.swift
//  fdmaps
//
//  Created by 吳天元 on 2016/5/10.
//  Copyright © 2016年 吳天元. All rights reserved.
//

import Foundation

class FDGMSearchHistory {
    static let folder = NSHomeDirectory() + "/Documents/History"
    static let path = folder + "/FDGoogleMapHistory"
    
    static func load(map: FDGoogleMap) -> Void {
        let manager = NSFileManager.defaultManager()
        if !manager.fileExistsAtPath(folder) {
            do {
                try manager.createDirectoryAtPath(folder, withIntermediateDirectories: true, attributes: nil)
            }
            catch {
                print("\(#file) \(#function) \(#line)")
            }
        }
        else if manager.fileExistsAtPath(path) {
            guard let dict = NSMutableArray(contentsOfFile: path) else {
                print("\(#file) \(#function) \(#line)")
                return
            }
            
            for object in dict {
                guard let obj = object as? NSDictionary else { continue }
                
                for (key, value) in obj {
                    guard let key = key as? String else { continue }
                    guard let value = value as? String else { continue }
                    let item = FDGMSearchItem(title: value, placeId: key)
                    map.searchResults.append(item)
                }
            }
        }
        else {
            /* nothing to be done */
        }
    }
    
    static func save(map: FDGoogleMap) -> Void {
        let array = NSMutableArray()
        for obj in map.searchResults {
            let dic = NSDictionary(dictionary: [obj.placeId: obj.title])
            array.addObject(dic)
        }
        array.writeToFile(path, atomically: true)
    }
}