//
//  FDPlaceItem.swift
//  fdmaps
//
//  Created by 吳天元 on 2016/5/10.
//  Copyright © 2016年 吳天元. All rights reserved.
//

import Foundation
import MapKit

enum FDPlaceItemType: Int {
    case Unknown = 0
    case ParkingLot = 1
}

class FDPlaceItem: CustomStringConvertible {
    let name: String
    let location: CLLocationCoordinate2D
    let type: FDPlaceItemType
    let id: Int
    
    init(id: Int, name: String, location: CLLocationCoordinate2D, type: FDPlaceItemType) {
        self.id = id
        self.name = name
        self.location = location
        self.type = type
    }
    
    var description: String {
        return self.name
    }
}