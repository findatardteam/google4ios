//
//  FDGMPlaceViewController.swift
//  FDMap
//
//  Created by 吳天元 on 5/16/16.
//  Copyright © 2016 FinData. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON

class FDGMPlaceViewController: UIViewController {
    var placeItem: FDPlaceItem!
    let mapView = GMSMapView()
    var firstShow = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.backgroundColor = UIColor.whiteColor()
        
        self.initTitle()
        self.initMapView()
        self.initButton()
    }
    
    func initTitle() -> Void {
        if let parkingItem = self.placeItem as? FDParkingLotPlaceItem {
            self.title = String(format: "%@ 空位：%d/%d 價格：%d", parkingItem.name, parkingItem.emptySpace, parkingItem.totalSpace, parkingItem.price)
        }
        else {
            self.title = placeItem.name
        }
    }
    
    func initMapView() -> Void {
        let h = self.navigationController!.navigationBar.frame.size.height
        let frame = self.view.frame
        self.mapView.frame = CGRectMake(0, h, frame.size.width, frame.size.height * 0.9 - h)
        self.view.addSubview(self.mapView)
        let camera = GMSCameraPosition.cameraWithLatitude(self.placeItem.location.latitude, longitude: self.placeItem.location.longitude, zoom: 15)
        self.mapView.camera = camera
        self.mapView.myLocationEnabled = true
        
        let marker = GMSMarker(position: self.placeItem.location)
        marker.title = self.placeItem.name
        marker.map = self.mapView
    }
    
    func initButton() -> Void {
        let frame = self.view.frame
        var button = UIButton()
        button.frame = CGRectMake(0, frame.size.height * 0.9, frame.size.width * 0.5, frame.size.height * 0.1)
        button.setTitle("Apple Map", forState: UIControlState.Normal)
        button.setTitleColor(UIColor.blueColor(), forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(self.onAppleMapButtonClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(button)
        
        button = UIButton()
        button.frame = CGRectMake(frame.size.width * 0.5, frame.size.height * 0.9, frame.size.width * 0.5, frame.size.height * 0.1)
        button.setTitle("Google Map", forState: UIControlState.Normal)
        button.setTitleColor(UIColor.blueColor(), forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(self.onGoogleMapButtonClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(button)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.mapView.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.New, context: nil)
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        self.mapView.removeObserver(self, forKeyPath: "myLocation")
    }
    
    func onAppleMapButtonClicked(button: UIButton) -> Void {
        
    }
    
    func onGoogleMapButtonClicked(button: UIButton) -> Void{
        
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if object === self.mapView {
            if keyPath == "myLocation" {
                if self.firstShow == true {
                    self.showRouting()
                    self.firstShow = false
                }
            }
        }
    }
    
    func showRouting() {
        guard let location = self.mapView.myLocation?.coordinate else { return }
        
        let urlString = String(format: "https://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&key=%@", location.latitude, location.longitude, self.placeItem!.location.latitude, self.placeItem!.location.longitude, FDGMSettings.shareInstance.GOOGLE_MAP_WEB_API_KEY)
        guard let url = NSURL(string: urlString) else { return }
        let session = NSURLSession(configuration:NSURLSessionConfiguration.defaultSessionConfiguration(), delegate: nil, delegateQueue: NSOperationQueue.mainQueue())
        session.dataTaskWithURL(url) { data, response, error in
            if let error = error {
                print("\(#file) \(#function) \(#line) \(error)")
                return
            }
            
            guard let data = data else { return }
            let json = JSON(data: data)
            
            let south = json["routes"][0]["bounds"]["southwest"]["lng"].double
            let west = json["routes"][0]["bounds"]["southwest"]["lat"].double
            let north = json["routes"][0]["bounds"]["northeast"]["lng"].double
            let east = json["routes"][0]["bounds"]["northeast"]["lat"].double
            let southwest = CLLocationCoordinate2D(latitude: west!, longitude: south!)
            let northeast = CLLocationCoordinate2D(latitude: east!, longitude: north!)
            let bounds = GMSCoordinateBounds(coordinate: southwest, coordinate: northeast)
            let camera = self.mapView.cameraForBounds(bounds, insets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
            self.mapView.camera = camera!
            
            for (_, step) in json["routes"][0]["legs"][0]["steps"] {
                let path = GMSPath(fromEncodedPath: step["polyline"]["points"].string!)
                let line = GMSPolyline(path: path)
                line.strokeWidth = 3.0
                line.map = self.mapView
            }
            
            }.resume()
    }
}