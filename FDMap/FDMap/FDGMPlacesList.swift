//
//  FDPlacesList.swift
//  FDMap
//
//  Created by 吳天元 on 5/13/16.
//  Copyright © 2016 FinData. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

class FDGMPlacesList: UIView, UIGestureRecognizerDelegate {
    var myLocation: CLLocationCoordinate2D?
    var searchDelegete: FDGMPlaceListSearchBarDelegete!
    var tableDelegate: FDGMPlaceListTableDelegete!
    var results = Array<FDPlaceItem>()
    var searchBar: UISearchBar = UISearchBar()
    var table: UITableView = UITableView()
    var mapView: GMSMapView!
    var viewController: UIViewController!
    
    override var frame: CGRect { didSet {
        self.table.frame = self.frame
        self.initView()
        } }
    
    init() {
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    private func initView() -> Void {
        self.searchDelegete = FDGMPlaceListSearchBarDelegete(list: self)
        self.searchBar.delegate = self.searchDelegete
        self.searchBar.scopeButtonTitles = ["距離", "空位", "價格"]
        self.searchBar.showsScopeBar = true
        self.searchBar.sizeToFit()
        
        
        self.tableDelegate = FDGMPlaceListTableDelegete(list: self)
        self.table.dataSource = tableDelegate
        self.table.delegate = tableDelegate
        self.table.tableHeaderView = self.searchBar
        self.addSubview(self.table)
        
        let tab = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(_:)))
        tab.delegate = self
        self.addGestureRecognizer(tab)
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        let indexPath = self.table.indexPathForRowAtPoint(touch.locationInView(self.table))
        return (indexPath === nil)
    }
    
    func updateList(location: CLLocationCoordinate2D?) {
        self.myLocation = location
        self.updateList()
    }
    
    func updateList() {
        var places = Array<FDPlaceItem>()
        for (_, place) in FDPlacesManager.shareInstance.places { places.append(place) }
        
        guard let mapView = self.mapView else {
            self.results = places
            self.table.reloadData()
            return;
        }
        
        if let marker = mapView.selectedMarker {
            self.results = self.sortAndFilter(marker.position, places: places)
        }
        else if let location = mapView.myLocation {
            self.results = self.sortAndFilter(location.coordinate, places: places)
        }
        else {
            self.results = places
        }
        self.table.reloadData()
    }
    
    func sortAndFilter(location: CLLocationCoordinate2D, places: Array<FDPlaceItem>) -> Array<FDPlaceItem> {
        return places.filter({ (item) -> Bool in
            return self.filterItem(item)
        }).sort({ (item1, item2) -> Bool in
            return self.sortItem(location, item1: item1, item2: item2)
        })
    }
    
    func filterItem(item: FDPlaceItem) -> Bool {
        guard let text = self.searchBar.text else { return true }
        if text.isEmpty { return true }
        return item.name.containsString(text)
    }
    
    func sortItem(location: CLLocationCoordinate2D, item1: FDPlaceItem, item2: FDPlaceItem) -> Bool {
        switch self.searchBar.selectedScopeButtonIndex {
        case 0:
            return self.sortItemByDistance(location, item1: item1, item2: item2)
            
        case 1:
            return self.sortItemByEmptySpace(item1, item2: item2)
            
        case 2:
            return self.sortItemByPrice(item1, item2: item2)
            
        default:
            return true
        }
    }
    
    func sortItemByDistance(fromLocation:CLLocationCoordinate2D, item1: FDPlaceItem, item2: FDPlaceItem) -> Bool {
        let location1 = CLLocation(latitude: item1.location.latitude, longitude: item1.location.longitude)
        let location2 = CLLocation(latitude: item2.location.latitude, longitude: item2.location.longitude)
        let location = CLLocation(latitude: fromLocation.latitude, longitude: fromLocation.longitude)
        return location.distanceFromLocation(location1) < location.distanceFromLocation(location2)
    }
    
    func sortItemByEmptySpace(item1: FDPlaceItem, item2: FDPlaceItem) -> Bool {
        guard let item1 = item1 as? FDParkingLotPlaceItem else { return false }
        guard let item2 = item2 as? FDParkingLotPlaceItem else { return true }
        
        if item1.emptySpace == item2.emptySpace {
            return item1.price < item2.price
        }
        else {
            return item1.emptySpace > item2.emptySpace
        }
    }
    
    func sortItemByPrice(item1: FDPlaceItem, item2: FDPlaceItem) -> Bool {
        guard let item1 = item1 as? FDParkingLotPlaceItem else { return false }
        guard let item2 = item2 as? FDParkingLotPlaceItem else { return true }
        
        if item1.price == item2.price {
            return item1.emptySpace > item2.emptySpace
        }
        else {
            return item1.price < item2.price
        }
    }
    
    func dismissKeyboard(sender: AnyObject) {
        self.searchBar.resignFirstResponder()
    }
    
}