//
//  FDParkingLotPlaceItem.swift
//  fdmaps
//
//  Created by 吳天元 on 2016/5/10.
//  Copyright © 2016年 吳天元. All rights reserved.
//

import Foundation
import MapKit
import SwiftyJSON

class FDParkingLotPlaceItem: FDPlaceItem {
    var price: Int!
    var start: NSTimeInterval!
    var end: NSTimeInterval!
    var emptySpace: Int!
    var totalSpace: Int!
    
    init(id: Int, name: String, location: CLLocationCoordinate2D, property: JSON) {
        super.init(id: id, name: name, location: location, type: FDPlaceItemType.ParkingLot)
        
        self.price = property["price"].int
        self.initBusinessHours(property)
    }
    
    private func initBusinessHours(property: JSON) -> Void {
        guard let startValue = property["business hours"][0].int else { return }
        guard let endValue = property["business hours"][1].int else { return }
        
        let startHour = startValue / 100
        let startMin = startValue % 100
        self.start = NSTimeInterval((startHour * 3600) + (startMin * 60))
        
        let endHour = endValue / 100
        let endMin = endValue % 100
        self.end = NSTimeInterval((endHour * 3600) + (endMin * 60))
        
        self.emptySpace = property["empty space"].int
        self.totalSpace = property["total"].int
    }
}