//
//  FDGoogleMap.swift
//  fdmaps
//
//  Created by 吳天元 on 2016/5/9.
//  Copyright © 2016年 吳天元. All rights reserved.
//

import GoogleMaps
import MapKit
import UIKit

class FDGoogleMap: BaseFDMap {
    let mapView = GMSMapView()
    let placesClient = GMSPlacesClient()
    var searchDelegete: FDGMSearchBarDelegete!
    var tableDelegate: FDGMSearchTableDelegete!
    var mapDelegate: FDGoogleMapDelegate!
    var searchResults = Array<FDGMSearchItem>()
    var firstShow = true
    var markers = Dictionary<Int, GMSMarker>()
    var viewController: UIViewController!
    override var frame: CGRect { didSet { self.mapView.frame = frame } }
    
    override init() {
        super.init(frame: CGRect.zero)
        self.initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.mapView.frame = frame
        self.initView()
    }
    
    private func initView() -> Void {
        self.loadHistory()
        self.initMapView()
        self.initDelegates()
    }
    
    private func initMapView() -> Void {
        self.mapDelegate = FDGoogleMapDelegate(map: self)
        self.mapView.myLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        self.mapView.delegate = self.mapDelegate
        self.insertSubview(self.mapView, atIndex: 0)
        self.mapView.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.New, context: nil)
    }
    
    private func initDelegates() -> Void {
        self.searchDelegete = FDGMSearchBarDelegete(map: self)
        self.searchBar.delegate = self.searchDelegete
        
        self.tableDelegate = FDGMSearchTableDelegete(map: self)
        self.resultsTable.dataSource = tableDelegate
        self.resultsTable.delegate = tableDelegate
    }
    
    func loadHistory() -> Void {
        FDGMSearchHistory.load(self)
    }
    
    func saveHistory() -> Void {
        FDGMSearchHistory.save(self)
    }
    
    deinit {
        self.mapView.removeObserver(self, forKeyPath: "myLocation")
    }
    
    func addHistory(history: FDGMSearchItem) {
        self.loadHistory()
        for obj in searchResults { if history.placeId == obj.placeId { return } }
        
        if self.searchResults.count >= 5 { self.searchResults.removeLast() }
        self.searchResults.insert(history, atIndex: 0)
        self.saveHistory()
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if object === self.mapView {
            if keyPath == "myLocation" {
                if self.firstShow == true {
                    guard let location = self.mapView.myLocation else { return }
                    let camera = GMSCameraPosition.cameraWithLatitude(location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 15)
                    self.mapView.camera = camera
                    firstShow = false
                }
            }
        }
    }
    
    func createMarker(item: FDPlaceItem) {
        let marker = GMSMarker(position: item.location)
        marker.title = item.name
        marker.map = self.mapView
        marker.userData = item
        marker.iconView = FDGMMarkerIconViewFactory.createIconView(item)
        self.markers[item.id] = marker
    }
    
}