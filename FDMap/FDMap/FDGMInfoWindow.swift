//
//  FDGMInfoWindow.swift
//  fdmaps
//
//  Created by 吳天元 on 5/12/16.
//  Copyright © 2016 吳天元. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON

class FDGMInfoWindow: UIView {
    var placeItem: FDPlaceItem! { didSet { self.initView() } }
    var mapView: GMSMapView!
    
    var title = UILabel()
    var content = UILabel()
    
    func initView() -> Void {
        self.backgroundColor = UIColor.whiteColor()
        self.frame = CGRectMake(0, 0, 200, 150)
        
        self.title.frame = CGRectMake(0, 0, 200, 50)
        self.title.textColor = UIColor.blueColor()
        self.title.text = self.placeItem.name
        self.title.backgroundColor = UIColor.yellowColor()
        self.title.textAlignment = NSTextAlignment.Center
        self.addSubview(self.title)
        
        self.content.frame = CGRectMake(0, 50, 200, 100)
        self.content.textColor = UIColor.blackColor()
        var text = "距離：-"
        text += "\n時間：-"
        self.content.text = text
        self.content.textAlignment = NSTextAlignment.Center
        self.content.numberOfLines = 3
        self.addSubview(self.content)
    }
    
    func initDistanceAndDuration(location: CLLocationCoordinate2D) {
        let urlString = String(format: "https://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&key=%@", location.latitude, location.longitude, self.placeItem!.location.latitude, self.placeItem!.location.longitude, FDGMSettings.shareInstance.GOOGLE_MAP_WEB_API_KEY)
        guard let url = NSURL(string: urlString) else { return }
        let session = NSURLSession(configuration:NSURLSessionConfiguration.defaultSessionConfiguration(), delegate: nil, delegateQueue: NSOperationQueue.mainQueue())
        session.dataTaskWithURL(url) { data, response, error in
            if let error = error {
                print("\(#file) \(#function) \(#line) \(error)")
                return
            }
            
            guard let data = data else { return }
            let json = JSON(data: data)
            let distance = json["routes"][0]["legs"][0]["distance"]["text"].string
            let duration = json["routes"][0]["legs"][0]["duration"]["text"].string
            self.updateDistanceAndDurationString(distance!, duration: duration!)
            
            /* reload info window */
            let marker = self.mapView.selectedMarker
            self.mapView.selectedMarker = nil
            self.mapView.selectedMarker = marker
            }.resume()
    }
    
    func updateDistanceAndDurationString(distance: String, duration: String) {
        var text = "距離：" + distance
        text += "\n時間：" + duration
        self.content.text = text
    }
    
}