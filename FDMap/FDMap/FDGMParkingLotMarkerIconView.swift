//
//  FDGMParkingLotMarkerIconView.swift
//  fdmaps
//
//  Created by 吳天元 on 5/11/16.
//  Copyright © 2016 吳天元. All rights reserved.
//

import UIKit

class FDGMParkingLotMarkerIconView: UIView {
    var background: UIView?
    var label: UILabel?
    
    var placeItem: FDParkingLotPlaceItem? {
        didSet {
            guard let item = self.placeItem else { return }
            
            if (self.background != nil) { self.background?.removeFromSuperview(); self.background = nil; }
            
            let imageView = UIImageView(image: UIImage(named: "placeholder-yellow"))
            self.insertSubview(imageView, atIndex: 0)
            self.frame = imageView.frame
            self.background = imageView
            
            if self.label == nil {
                let label = UILabel()
                label.frame = CGRectMake(0, 0, self.frame.width, self.frame.width * 0.75)
                label.textColor = UIColor(colorLiteralRed: 0, green: 0.5, blue: 0, alpha: 1)
                label.textAlignment = NSTextAlignment.Center
                label.font = UIFont.boldSystemFontOfSize(10)
                self.addSubview(label)
                self.label = label
            }
            
            guard let price = item.price else {
                self.label?.text = "?"
                return
            }
            
            self.label?.text = String(price)
        }
    }
    
}
