//
//  FDPlaceItemFactory.swift
//  fdmaps
//
//  Created by 吳天元 on 5/11/16.
//  Copyright © 2016 吳天元. All rights reserved.
//

import MapKit
import SwiftyJSON

class FDPlaceItemFactory {
    static func createItem(location: CLLocationCoordinate2D, property: JSON) -> FDPlaceItem? {
        guard let name = property["name"].string else { return nil }
        guard let typeValue = property["type"].int else { return nil }
        guard let id = property["id"].int else { return nil }
        
        guard let type = FDPlaceItemType(rawValue: typeValue) else {
            return FDPlaceItem(id: id, name: name, location: location, type: FDPlaceItemType.Unknown)
        }
        
        switch type {
        case FDPlaceItemType.ParkingLot:
            return FDParkingLotPlaceItem(id: id, name: name, location: location, property: property)
        default:
            return FDPlaceItem(id: id, name: name, location: location, type: FDPlaceItemType.Unknown)
        }
    }
}