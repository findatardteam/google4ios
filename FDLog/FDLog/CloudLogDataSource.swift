//
//  CloudLogDataSource.swift
//  Log
//
//  Created by Wu Tien Yuan on 2016/4/22.
//  Copyright © 2016年 Eskygo. All rights reserved.
//

import UIKit

class CloudLogDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    let table: UITableView
    var timer: NSTimer?
    var observers = Array<NSObjectProtocol>()
    
    init(table: UITableView) {
        self.table = table
        super.init()
        
        self.initObservers()
        LogManager.shareInstance.readCloudLogs(NSDate())
    }
    
    deinit {
        let center = NSNotificationCenter.defaultCenter()
        for obj in self.observers {
            center.removeObserver(obj)
        }
        self.observers.removeAll()
    }
    
    func initObservers() {
        let center = NSNotificationCenter.defaultCenter()
        var object = center.addObserverForName(CLOUD_LOGGER_NOTIFICATION.READ_LOGS_COMPLETE.rawValue, object: nil, queue: NSOperationQueue.mainQueue(), usingBlock: {(notification: NSNotification) -> Void in
            self.readLogsComplete()
        } )
        self.observers.append(object)
        
        object = center.addObserverForName(CLOUD_LOGGER_NOTIFICATION.READ_LOGS_FAILED.rawValue, object: nil, queue: NSOperationQueue.mainQueue(), usingBlock: {(notification: NSNotification) -> Void in
            self.readLogsComplete()
        } )
        self.observers.append(object)
    }
    
    func readLogsComplete() -> Void {
        self.table.reloadData()
        self.updateLogView()
        
        if timer != nil { timer!.invalidate() }
        
        timer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(self.readLogs), userInfo: nil, repeats: false)
    }
    
    func readLogFailed() -> Void {
        if timer != nil { timer!.invalidate() }
        
        timer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(self.readLogs), userInfo: nil, repeats: false)
    }
    
    func readLogs() -> Void {
        LogManager.shareInstance.readCloudLogs(NSDate())
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LogManager.shareInstance.getCloudLogsBuffer().count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("LogCellItem")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "LogCellItem")
        }
        let logs = LogManager.shareInstance.getCloudLogsBuffer()
        cell!.textLabel!.text = self.logItemToString(logs[indexPath.row])
        cell!.textLabel!.textColor = UIColor.blueColor()
        cell!.textLabel!.font = UIFont.systemFontOfSize(10)
        cell!.textLabel!.numberOfLines = 3
        return cell!;
    }
    
    private func logItemToString(item: LogItem) -> String {
        return String(format: "%@ User:%@ Sender:%@ Excute Time:%f Location:(%f, %f), Event:%@ Data:%@\n", self.dateToString(item.timestamp), item.user, item.sender, item.executeTime, item.locationX, item.locationY, item.event, item.data)
    }
    
    private func dateToString(date: NSDate) -> String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.stringFromDate(date)
    }
    
    private func updateLogView() {
        self.table.reloadData()
        let rect = CGRect(x: 0, y: self.table.contentSize.height - self.table.bounds.height, width: self.table.contentSize.width, height: self.table.bounds.height)
        self.table.scrollRectToVisible(rect, animated: false)
    }
}