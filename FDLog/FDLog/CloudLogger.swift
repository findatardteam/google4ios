//
//  CloudLogger.swift
//  Log
//
//  Created by Wu Tien Yuan on 2016/4/14.
//  Copyright © 2016年 Eskygo. All rights reserved.
//

import Foundation

class CloudLogger {
    static let logglyUrlString = "https://logs-01.loggly.com/bulk/b3d0aee5-532f-4de3-9dad-b31719488268/tag/Log/"
    static let USER_ID = "newtype"
    static let PASSWORD = "Eskygo27928278"
    
    var enabled = false
    let authorization: String
    var buffer = Array<LogItem>()
    var index:UInt = 0
    
    
    init() {
        let authStr = CloudLogger.USER_ID + ":" + CloudLogger.PASSWORD;
        let authData = authStr.dataUsingEncoding(NSUTF8StringEncoding)
        let value = authData?.base64EncodedStringWithOptions([])
        self.authorization = "Basic " + value!
    }
    
    func addLog(item: LogItem) -> Void {
        if self.enabled == false { return }
        
        do {
            guard let url = NSURL(string: CloudLogger.logglyUrlString) else {
                print("Error: \(#file) \(#function) \(#line)")
                return
            }
            let request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "POST"
            request.HTTPBody = try logItemToJson(item)
            
            let key = "com.findata.log.\(self.index)"
            self.index = (self.index + 1) % UInt(UINT16_MAX)
            let config: NSURLSessionConfiguration = NSURLSessionConfiguration.backgroundSessionConfigurationWithIdentifier(key)
            let session = NSURLSession(configuration: config)
            let task = session.downloadTaskWithRequest(request)
            task.resume()
        }
        catch {
            print("Error: \(#file) \(#function) \(#line) :\(error)")
        }
    }
    
    private func addLogCompletionHandler(data: NSData?, response: NSURLResponse?, error: NSError?) -> Void {
        
        if let error = error {
            print("Error: \(#file) \(#function) \(#line): \(error)")
            return
        }
        
        guard let data = data else {
            print("Error: \(#file) \(#function) \(#line)")
            return
        }
        
        do {
            guard let dict = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String: String] else {
                print("Error: \(#file) \(#function) \(#line)")
                return
            }
            
            if dict["response"] != "ok" {
                print("Error: \(#file) \(#function) \(#line)")
            }
        }
        catch {
            print("Error: \(#file) \(#function) \(#line): \(error)")
        }
    }
    
    func getLogsBuffer() -> Array<LogItem> {
        return self.buffer
    }
    
    func readLogs(date: NSDate) -> Void {
        let sec: Int = Int(floor(date.timeIntervalSince1970))
        let from = sec - (sec % 86400)
        let until = from + 86400
        let now: Int = Int(floor(NSDate().timeIntervalSince1970))
        let fromSec: Int = from - now
        let untilSec: Int = until - now
        
        let url = self.createRequestRsidUrl(fromSec, until:  untilSec)
        self.requestUrl(url, parsing: self.parseRsidJsonData)
    }
    
    private func createRequestRsidUrl(from: Int, until: Int) -> String {
        if until > 0 {
            return String(format: "http://newtype.loggly.com/apiv2/search?q=*p&from=%ds&until=now&size=100", from)
        }
        else {
            return String(format: "http://newtype.loggly.com/apiv2/search?q=*p&from=%ds&until=%ds&size=100", from, until)
        }
    }
    
    private func requestUrl(url: String, parsing: (data: NSData) -> Void) -> Void {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.setValue(self.authorization, forHTTPHeaderField: "Authorization")
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: {
            (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            self.completionHandler(data, response: response, error: error, parsing: parsing)
        })
        task.resume()
    }
    
    private func completionHandler(data: NSData?, response: NSURLResponse?, error: NSError?, parsing: (data: NSData) -> Void) -> Void {
        if let error = error {
            print("Error: \(#file) \(#function) \(#line) \(error)")
            NSNotificationCenter.defaultCenter().postNotificationName(CLOUD_LOGGER_NOTIFICATION.READ_LOGS_FAILED.rawValue, object: nil)
            return
        }
        
        guard let data = data else {
            print("Error: \(#file) \(#function) \(#line)")
            NSNotificationCenter.defaultCenter().postNotificationName(CLOUD_LOGGER_NOTIFICATION.READ_LOGS_FAILED.rawValue, object: nil)
            return
        }
        
        //let string = NSString(data: data, encoding: NSUTF8StringEncoding)
        //print("\(string)")
        parsing(data: data)
    }
    
    private func parseRsidJsonData(data: NSData) {
        do {
            guard let dict = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject] else {
                NSLog("Error: \(#file) \(#function) \(#line)")
                NSNotificationCenter.defaultCenter().postNotificationName(CLOUD_LOGGER_NOTIFICATION.READ_LOGS_FAILED.rawValue, object: nil)
                return
            }
            
            guard let rsid = dict["rsid"] as? [String:AnyObject] else {
                NSLog("Error: \(#file) \(#function) \(#line)")
                NSNotificationCenter.defaultCenter().postNotificationName(CLOUD_LOGGER_NOTIFICATION.READ_LOGS_FAILED.rawValue, object: nil)
                return
            }
            
            guard let id = rsid["id"] as? String else {
                NSLog("Error: \(#file) \(#function) \(#line)")
                NSNotificationCenter.defaultCenter().postNotificationName(CLOUD_LOGGER_NOTIFICATION.READ_LOGS_FAILED.rawValue, object: nil)
                return
            }
            
            let url = "http://newtype.loggly.com/apiv2/events?rsid=" + String(id)
            self.requestUrl(url, parsing: self.parseLogsJsonData)
        }
        catch {
            NSLog("\(#file) \(#function) \(#line) \(error)")
            NSNotificationCenter.defaultCenter().postNotificationName(CLOUD_LOGGER_NOTIFICATION.READ_LOGS_FAILED.rawValue, object: nil)
        }
    }
    
    private func parseLogsJsonData(data: NSData) {
        do {
            guard let dict = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String: AnyObject] else {
                print("Error \(#file) \(#function) \(#line)")
                NSNotificationCenter.defaultCenter().postNotificationName(CLOUD_LOGGER_NOTIFICATION.READ_LOGS_FAILED.rawValue, object: nil)
                return
            }
            
            guard let total_events = dict["total_events"] as? Int else {
                print("Error: \(#file) \(#function) \(#line)")
                NSNotificationCenter.defaultCenter().postNotificationName(CLOUD_LOGGER_NOTIFICATION.READ_LOGS_FAILED.rawValue, object: nil)
                return
            }
            
            if total_events == 0 {
                NSNotificationCenter.defaultCenter().postNotificationName(CLOUD_LOGGER_NOTIFICATION.READ_LOGS_COMPLETE.rawValue, object: nil)
                return
            }
            
            guard let events = dict["events"] as? Array<AnyObject> else {
                print("Error: \(#file) \(#function) \(#line)")
                NSNotificationCenter.defaultCenter().postNotificationName(CLOUD_LOGGER_NOTIFICATION.READ_LOGS_FAILED.rawValue, object: nil)
                return
            }
            
            var logs = Array<LogItem>()
            for obj in events {
                guard let value = obj as? [String: AnyObject] else { continue }
                guard let event = value["event"] as? [String: AnyObject] else { continue }
                guard let json = event["json"] as? [String: AnyObject] else { continue }
                do {
                    let item = try DictionaryToLogItem(json)
                    logs.append(item)
                }
                catch {
                    print("Error: \(#file) \(#function) \(#line) \(error)")
                }
            }
            
            self.buffer = logs
            NSNotificationCenter.defaultCenter().postNotificationName(CLOUD_LOGGER_NOTIFICATION.READ_LOGS_COMPLETE.rawValue, object: nil)
        }
        catch {
            print("Error: \(#file) \(#function) \(#line) \(error)")
            NSNotificationCenter.defaultCenter().postNotificationName(CLOUD_LOGGER_NOTIFICATION.READ_LOGS_FAILED.rawValue, object: nil)
        }
    }
    
    private func logItemToString(item: LogItem) -> String {
        return String(format: "%@ User:%@ Sender:%@ Excute Time:%f Location:(%f, %f), Event:%@ Data:%@", self.dateToString(item.timestamp), item.user, item.sender, item.executeTime, item.locationX, item.locationY, item.event, item.data)
    }
    
    private func dateToString(date: NSDate) -> String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.stringFromDate(date)
    }
}