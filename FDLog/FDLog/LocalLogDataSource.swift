//
//  LogDataSource.swift
//  Log
//
//  Created by Wu Tien Yuan on 2016/4/18.
//  Copyright © 2016年 Eskygo. All rights reserved.
//

import UIKit

class LocalLogDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    let viewController: UIViewController
    
    init(vc: UIViewController) {
        self.viewController = vc
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LogManager.shareInstance.readLocalLogs(NSDate()).count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let logs = LogManager.shareInstance.readLocalLogs(NSDate())
        
        var cell = tableView.dequeueReusableCellWithIdentifier("LogCellItem")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "LogCellItem")
        }
        cell!.textLabel!.text = self.logItemToString(logs[indexPath.row])
        cell!.textLabel!.textColor = UIColor.blueColor()
        cell!.textLabel!.font = UIFont.systemFontOfSize(10)
        cell!.textLabel!.numberOfLines = 3
        return cell!;
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            LogManager.shareInstance.deleteLocalLog(NSDate(), index: indexPath.row)
            tableView.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        let vc = LogEditorViewController()
        vc.index = indexPath.row
        self.viewController.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func logItemToString(item: LogItem) -> String {
        return String(format: "%@ User:%@ Sender:%@ Excute Time:%f Location:(%f, %f), Event:%@ Data:%@\n", self.dateToString(item.timestamp), item.user, item.sender, item.executeTime, item.locationX, item.locationY, item.event, item.data)
    }
    
    private func dateToString(date: NSDate) -> String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.stringFromDate(date)
    }
}
