//
//  LogManager.swift
//  Log
//
//  Created by Wu Tien Yuan on 2016/4/14.
//  Copyright © 2016年 Eskygo. All rights reserved.
//

import Foundation

class LogManager {
    static let shareInstance = LogManager()
    var level: LOGGER_LEVEL = LOGGER_LEVEL.DEBUG
    
    let localLogger = LocalLogger()
    let cloudLogger = CloudLogger()
    
    private init() {
        localLogger.enabled = true
        cloudLogger.enabled = true
    }
    
    func addLog(item : LogItem) -> Void {
        if (self.logLevelToValue(self.level) > self.logLevelToValue(item.level)) { return }
        
        localLogger.addLog(item)
        cloudLogger.addLog(item)
    }
    
    func readLocalLogs(date: NSDate) -> Array<LogItem> {
        return localLogger.readLogs(date)
    }
    
    func updateLocalLog(date: NSDate, index: Int, item: LogItem) -> Void {
        localLogger.updateLog(date, index: index, item: item)
    }
    
    func deleteLocalLog(date: NSDate, index: Int) -> Void {
        localLogger.deleteLog(date, index: index)
    }
    
    func readCloudLogs(date: NSDate) {
        cloudLogger.readLogs(date)
    }
    
    func getCloudLogsBuffer() -> Array<LogItem> {
        return cloudLogger.getLogsBuffer()
    }
    
    private func logLevelToValue(level: LOGGER_LEVEL) -> Int {
        switch level {
        case LOGGER_LEVEL.TRACE: return 100
        case LOGGER_LEVEL.DEBUG: return 200
        case LOGGER_LEVEL.INFO: return 300
        case LOGGER_LEVEL.WARNING: return 400
        case LOGGER_LEVEL.ERROR: return 500
        case LOGGER_LEVEL.FATAL: return 600
            
        }
    }
}
