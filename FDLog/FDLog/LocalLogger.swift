//
//  LocalLogger.swift
//  Log
//
//  Created by Wu Tien Yuan on 2016/4/14.
//  Copyright © 2016年 Eskygo. All rights reserved.
//

import Foundation

class LocalLogger {
    var enabled = false
    var level = LOGGER_LEVEL.DEBUG
    
    let folder: String
    let basePath: String
    var path: String
    var date = NSDate()
    
    
    init () {
        folder = NSHomeDirectory() + "/Documents/Log"
        basePath = folder + "/log_db"
        path = basePath + LocalLogger.dateToString(self.date)
        let manager = NSFileManager.defaultManager()
        if !manager.fileExistsAtPath(folder) {
            do {
                try manager.createDirectoryAtPath(folder, withIntermediateDirectories: true, attributes: nil)
            }
            catch {
                NSLog("%@ %@ %d: create folder exception", #file, #function, #line)
            }
        }
    }
    
    func addLog(item : LogItem) -> Void {
        if !self.enabled { return }
        
        objc_sync_enter(self.basePath)
        self.doAddLog(item)
        objc_sync_exit(self.basePath)
    }
    
    private func doAddLog(item: LogItem) {
        if self.level.rawValue > item.level.rawValue { return }
        self.updatePath()
        do {
            let data = try logItemToJson(item)
            guard let string = NSString(data: data, encoding: NSUTF8StringEncoding) else {
                print("Error: \(#file) \(#function) \(#line)")
                return
            }
            self.appendStringToFile(((string as String) + "\n"));
        }
        catch {
            print("Error: \(#file) \(#function) \(#line) \(error)")
        }
    }
    
    private func updatePath() {
        let date = NSDate()
        if !NSCalendar.currentCalendar().isDate(self.date, inSameDayAsDate: date) {
            path = basePath + LocalLogger.dateToString(self.date)
            self.date = date
        }
    }
    
    private func appendStringToFile(text: String) {
        let manager = NSFileManager.defaultManager()
        if !manager.fileExistsAtPath(self.path) {
            do {
                try text.writeToFile(self.path, atomically: false, encoding: NSUTF8StringEncoding)
            }
            catch {
                NSLog("%@ %@ %d: write file failed: %@", #file, #function, #line, self.path)
            }
        }
        else {
            guard let data = text.dataUsingEncoding(NSUTF8StringEncoding) else {
                NSLog("%@ %@ %d: convert text to data failed: %@", #file, #function, #line, text)
                return
            }
            
            guard let handle = NSFileHandle(forWritingAtPath: self.path) else {
                NSLog("%@ %@ %d: open file failed: %@", #file, #function, #line, self.path)
                return
            }
            
            handle.seekToEndOfFile()
            handle.writeData(data)
            handle.closeFile()
        }
    }
    
    func readLogs(date: NSDate) -> Array<LogItem> {
        objc_sync_enter(self.basePath)
        let logs = self.doReadLogs(date)
        objc_sync_exit(self.basePath)
        return logs
    }
    
    func doReadLogs(date: NSDate) -> Array<LogItem> {
        var items = Array<LogItem>();
        let path = basePath + LocalLogger.dateToString(date)
        if !NSFileManager.defaultManager().fileExistsAtPath(path) { return items }
        do {
            let text = try String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
            let lines = text.componentsSeparatedByString("\n")
            for line in lines {
                if line.characters.count == 0 { continue }
                do {
                    let item = try JsonToLogItem(line)
                    items.append(item)
                }
                catch {
                    print("Error \(#file) \(#function) \(#line): \(error)")
                }
            }
        }
        catch {
            print("Error \(#file) \(#function) \(#line): \(error)")
        }
        return items
    }
    
    func updateLog(date: NSDate, index: Int, item: LogItem) {
        objc_sync_enter(self.basePath)
        self.doUpdateLog(date, index: index, item: item)
        objc_sync_exit(self.basePath)
    }
    
    private func doUpdateLog(date: NSDate, index: Int, item: LogItem) {
        let manager = NSFileManager.defaultManager()
        let path = self.basePath + LocalLogger.dateToString(date)
        if manager.fileExistsAtPath(path) == false { return }
        
        do {
            var text = try String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
            var lines = text.componentsSeparatedByString("\n")
            if index < 0 { return }
            if index > (lines.count - 1) { return }
            
            let data = try logItemToJson(item)
            guard let string = NSString(data: data, encoding: NSUTF8StringEncoding) else {
                print("Error: \(#file) \(#function) \(#line)")
                return
            }
            
            lines[index] = string as String
            text = lines.joinWithSeparator("\n")
            
            do {
                try text.writeToFile(path, atomically: false, encoding: NSUTF8StringEncoding)
            }
            catch {
                print("Error \(#file) \(#function) \(#line): \(error)")
            }
        }
        catch {
            print("Error \(#file) \(#function) \(#line): \(error)")
        }
    }
    
    func deleteLog(date: NSDate, index: Int) {
        objc_sync_enter(self.basePath)
        self.doDeleteLog(date, index: index)
        objc_sync_exit(self.basePath)
    }
    
    func doDeleteLog(date: NSDate, index: Int) {
        let manager = NSFileManager.defaultManager()
        let path = self.basePath + LocalLogger.dateToString(date)
        if manager.fileExistsAtPath(path) == false { return }
        
        do {
            var text = try String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
            var lines = text.componentsSeparatedByString("\n")
            if index < 0 { return }
            if index > (lines.count - 1) { return }
            
            lines.removeAtIndex(index)
            text = lines.joinWithSeparator("\n")
            
            do {
                try text.writeToFile(path, atomically: false, encoding: NSUTF8StringEncoding)
            }
            catch {
                NSLog("%@ %@ %d: write log from file failed: %@", #file, #function, #line, path)
            }
        }
        catch {
            NSLog("%@ %@ %d: read log from file failed: %@", #file, #function, #line, path)
        }
    }
    
    func clear() {
        objc_sync_enter(self.basePath)
        self.doClear()
        objc_sync_exit(self.basePath)
    }
    
    func doClear() {
        let manager = NSFileManager.defaultManager()
        do {
            let files = try manager.contentsOfDirectoryAtPath(self.folder)
            for file in files {
                let path = folder.stringByAppendingFormat("/%@", file)
                try manager.removeItemAtPath(path)
            }
        }
        catch {
            NSLog("%@ %@ %d: clean failed", #file, #function, #line)
        }
    }
    
    static func dateToString(date: NSDate = NSDate()) -> String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "-yyyy-MM-dd"
        return formatter.stringFromDate(date)
    }
}