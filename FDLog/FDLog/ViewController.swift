//
//  ViewController.swift
//  Log
//
//  Created by Wu Tien Yuan on 2016/4/14.
//  Copyright © 2016年 Eskygo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let localLogView = UITableView()
    let cloudLogView = UITableView()
    var localLogViewDataSource: LocalLogDataSource?
    var cloudLogViewDataSource: CloudLogDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        var button = UIButton()
        button.frame = CGRectMake(0, 0, self.view.frame.size.width * 0.5, 100)
        button.setTitle("Button 01", forState: UIControlState.Normal)
        button.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        button.addTarget(self, action:#selector(ViewController.onButton1Clicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(button)
        
        button = UIButton()
        button.frame = CGRectMake(self.view.frame.size.width * 0.5, 0, self.view.frame.size.width * 0.5, 100)
        button.setTitle("Button 02", forState: UIControlState.Normal)
        button.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        button.addTarget(self, action:#selector(ViewController.onButton2Clicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(button)
        
        
        self.localLogViewDataSource = LocalLogDataSource(vc: self)
        self.localLogView.frame = CGRectMake(0, self.view.frame.height * 0.5, self.view.frame.width * 0.5, self.view.frame.height * 0.5)
        self.localLogView.dataSource = self.localLogViewDataSource
        self.localLogView.delegate = self.localLogViewDataSource
        self.updateLogView()
        self.view.addSubview(self.localLogView)
        
        self.cloudLogViewDataSource = CloudLogDataSource(table: self.cloudLogView)
        self.cloudLogView.frame = CGRectMake(self.view.frame.width * 0.5, self.view.frame.height * 0.5, self.view.frame.width * 0.5, self.view.frame.height * 0.5)
        self.cloudLogView.dataSource = self.localLogViewDataSource
        self.cloudLogView.delegate = self.localLogViewDataSource
        self.updateLogView()
        self.view.addSubview(self.cloudLogView)
        
        //let logger = CloudLogger()
        //logger.readLogs(NSDate())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.localLogView.reloadData()
        
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewDidDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
        
        super.viewDidDisappear(animated)
    }

    func onButton1Clicked(sender: UIButton) -> Void {
        let manager = LogManager.shareInstance
        let item = LogItem(level: LOGGER_LEVEL.DEBUG, user: "User", sender: "Button 01", executeTime: 0, locationX: 0, locationY: 0, event: "TouchUpInside", data: "Data")
        manager.addLog(item)
        
        self.updateLogView()
    }
    
    func onButton2Clicked(sender: UIButton) -> Void {
        let manager = LogManager.shareInstance
        let item = LogItem(level: LOGGER_LEVEL.DEBUG, user: "User", sender: "Button 02", executeTime: 0, locationX: 0, locationY: 0, event: "TouchUpInside", data: "Data")
        manager.addLog(item)
        
        self.updateLogView()
    }

    private func updateLogView() {
        self.localLogView.reloadData()
        let rect = CGRect(x: 0, y: self.localLogView.contentSize.height - self.localLogView.bounds.height, width: self.localLogView.contentSize.width, height: self.localLogView.bounds.height)
        self.localLogView.scrollRectToVisible(rect, animated: false)
    }
}

