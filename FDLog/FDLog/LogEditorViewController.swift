//
//  LogEditViewController.swift
//  Log
//
//  Created by Wu Tien Yuan on 2016/4/18.
//  Copyright © 2016年 Eskygo. All rights reserved.
//

import UIKit

class LogEditorViewController: UIViewController {
    var index: Int = -1
    let tfUser = UITextField()
    let tfEvent = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.view.backgroundColor = UIColor.whiteColor()
        let logs = LogManager.shareInstance.readLocalLogs(NSDate())
        let item = logs[self.index]
        
        var label = UILabel()
        label.frame = CGRectMake(0, 50, 100, 50)
        label.text = "User"
        label.textColor = UIColor.blueColor()
        self.view.addSubview(label)
        
        tfUser.frame = CGRectMake(100, 50, 500, 50)
        tfUser.text = item.user
        tfUser.textColor = UIColor.blackColor()
        self.view.addSubview(tfUser)
        
        label = UILabel()
        label.frame = CGRectMake(0, 100, 100, 50)
        label.text = "Event"
        label.textColor = UIColor.blueColor()
        self.view.addSubview(label)
        
        tfEvent.frame = CGRectMake(100, 100, 500, 50)
        tfEvent.text = item.event
        tfEvent.textColor = UIColor.blackColor()
        self.view.addSubview(tfEvent)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        let logs = LogManager.shareInstance.readLocalLogs(NSDate())
        let item = logs[self.index]
        
        let newItem = LogItem(user: self.tfUser.text!, sender: item.sender, executeTime: item.executeTime, locationX: item.locationX, locationY: item.locationY, event: self.tfEvent.text!, data: item.data)
        
        LogManager.shareInstance.updateLocalLog(item.timestamp, index: self.index, item: newItem)
        
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        
        super.viewDidDisappear(animated)
    }
}

