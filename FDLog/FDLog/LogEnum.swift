//
//  LogEnum.swift
//  Log
//
//  Created by Wu Tien Yuan on 2016/4/22.
//  Copyright © 2016年 Eskygo. All rights reserved.
//

import Foundation

enum LOGGER_LEVEL: String {
    case TRACE = "TRACE"
    case DEBUG = "DEBUG"
    case INFO = "INFO"
    case WARNING = "WARNING"
    case ERROR = "ERROR"
    case FATAL = "FATAL"
}

enum LOGITEM_ERROR: ErrorType {
    case CONVERT_DICTIONARY
    case CONVERT_STRING
}

enum CLOUD_LOGGER_NOTIFICATION: String {
    case READ_LOGS_COMPLETE = "CLOUD_LOGGER_NOTIFICATION_READ_LOGS_COMPLETE"
    case READ_LOGS_FAILED = "CLOUD_LOGGER_NOTIFICATION_READ_LOGS_FAILED"
}