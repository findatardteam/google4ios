//
//  LogItem.swift
//  Log
//
//  Created by Wu Tien Yuan on 2016/4/14.
//  Copyright © 2016年 Eskygo. All rights reserved.
//

import Foundation

struct LogItem {
    let version: Int
    let level: LOGGER_LEVEL
    let user: String
    let sender: String
    let timestamp: NSDate
    let executeTime: NSTimeInterval
    let locationX: Double
    let locationY: Double
    let event: String
    let data: String
    
    init(version: Int = 1, level: LOGGER_LEVEL = LOGGER_LEVEL.TRACE, user: String, sender: String, timestamp: NSDate = NSDate(), executeTime: NSTimeInterval, locationX: Double, locationY: Double, event: String, data: String) {
        self.version = version
        self.level = level
        self.user = user
        self.sender = sender
        self.timestamp = NSDate(timeIntervalSince1970: floor(timestamp.timeIntervalSince1970))
        self.executeTime = executeTime
        self.locationX = locationX
        self.locationY = locationY
        self.event = event
        self.data = data
    }
}

extension LogItem: Equatable {}

func ==(lhs: LogItem, rhs: LogItem) -> Bool {
    if lhs.version != rhs.version { return false }
    if lhs.level != rhs.level { return false }
    if lhs.user != rhs.user { return false }
    if lhs.sender != rhs.sender { return false }
    if abs(lhs.timestamp.timeIntervalSince1970 - rhs.timestamp.timeIntervalSince1970) > 1.0e-6 { return false }
    if lhs.executeTime != rhs.executeTime { return false }
    if lhs.locationX != rhs.locationX { return false }
    if lhs.locationY != rhs.locationY { return false }
    if lhs.event != rhs.event { return false }
    if lhs.data != rhs.data { return false }
    return true
}

func logItemToJson(item: LogItem) throws -> NSData {
    let dict: [String:String] = ["version":String(item.version),
                                 "level":item.level.rawValue,
                                 "user":item.user,
                                 "sender":item.sender,
                                 "timestamp":dateToString(item.timestamp),
                                 "executeTime":String(item.executeTime),
                                 "locationX":String(item.locationX),
                                 "locationY":String(item.locationY),
                                 "event":item.event,
                                 "data":item.data]
    do {
        return try NSJSONSerialization.dataWithJSONObject(dict, options: [])
    }
    catch {
        throw error
    }
}

func JsonToLogItem(string: String) throws -> LogItem {
    do {
        guard let data = NSString(string: string).dataUsingEncoding(NSUTF8StringEncoding) else {
            throw LOGITEM_ERROR.CONVERT_STRING
        }
        guard let dict = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String: AnyObject] else {
            throw LOGITEM_ERROR.CONVERT_STRING
        }
        return try DictionaryToLogItem(dict)
    }
    catch {
        throw error
    }
}

func DictionaryToLogItem(dict: [String: AnyObject]) throws -> LogItem {
    guard let versionString = dict["version"] as? String else { throw LOGITEM_ERROR.CONVERT_DICTIONARY }
    let version = NSString(string: versionString).integerValue
    guard let levelString = dict["level"] as? String else { throw LOGITEM_ERROR.CONVERT_DICTIONARY }
    guard let level = LOGGER_LEVEL(rawValue: levelString) else { throw LOGITEM_ERROR.CONVERT_DICTIONARY }
    guard let user = dict["user"] as? String else { throw LOGITEM_ERROR.CONVERT_DICTIONARY }
    guard let sender = dict["sender"] as? String else { throw LOGITEM_ERROR.CONVERT_DICTIONARY }
    guard let timestampString = dict["timestamp"] as? String else { throw LOGITEM_ERROR.CONVERT_DICTIONARY }
    guard let timestamp = stringToDate(timestampString) else { throw LOGITEM_ERROR.CONVERT_DICTIONARY }
    guard let executeTimeString = dict["executeTime"] as? String else { throw LOGITEM_ERROR.CONVERT_DICTIONARY }
    let executeTime = NSString(string: executeTimeString).doubleValue
    guard let locationXString = dict["locationX"] as? String else { throw LOGITEM_ERROR.CONVERT_DICTIONARY }
    let locationX = NSString(string: locationXString).doubleValue
    guard let locationYString = dict["locationY"] as? String else { throw LOGITEM_ERROR.CONVERT_DICTIONARY }
    let locationY = NSString(string: locationYString).doubleValue
    guard let event = dict["event"] as? String else { throw LOGITEM_ERROR.CONVERT_DICTIONARY }
    guard let data = dict["data"] as? String else { throw LOGITEM_ERROR.CONVERT_DICTIONARY }
    return LogItem(version: version, level: level, user: user, sender: sender, timestamp:timestamp, executeTime: executeTime, locationX: locationX, locationY: locationY, event: event, data: data)
}

func dateToString(date: NSDate) -> String {
    let formatter = NSDateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return formatter.stringFromDate(date)
}

func stringToDate(string: String) -> NSDate? {
    let formatter = NSDateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return formatter.dateFromString(string)
}
