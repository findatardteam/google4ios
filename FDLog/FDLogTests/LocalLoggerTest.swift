//
//  LocalLoggerTest.swift
//  Log
//
//  Created by Wu Tien Yuan on 2016/4/14.
//  Copyright © 2016年 Eskygo. All rights reserved.
//

import XCTest
@testable import FDLog

class LocalLoggerTests: XCTestCase {
    var logger: LocalLogger!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        logger = LocalLogger()
        logger.clear()
        logger.enabled = true
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        logger = nil
        NSThread.sleepForTimeInterval(0.1)
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func test_addLog() {
        var item = LogItem(level: LOGGER_LEVEL.DEBUG, user: "User", sender: "Sender", executeTime: 0, locationX: 0, locationY: 0, event: "Event", data: "Data")
        logger.addLog(item)
        
        var logs = logger.readLogs(NSDate())
        XCTAssertEqual(logs.count, 1)
        
        
        item = LogItem(level: LOGGER_LEVEL.DEBUG, user: "User1", sender: "Sender", executeTime: 0, locationX: 0, locationY: 0, event: "Event", data: "Data")
        logger.addLog(item)
        logs = logger.readLogs(NSDate())
        XCTAssertEqual(logs.count, 2)
        XCTAssertEqual(item, logs[1])
        XCTAssertNotEqual(item, logs[0])
    }
    
    func test_readLogs() {
        let item = LogItem(level: LOGGER_LEVEL.DEBUG, user: "User", sender: "Sender", executeTime: 0, locationX: 0, locationY: 0, event: "Event", data: "Data")
        logger.addLog(item)
        logger.addLog(item)
        logger.addLog(item)
        logger.addLog(item)
        logger.addLog(item)
        
        let logs = logger.readLogs(NSDate())
        XCTAssertEqual(logs.count, 5)
        XCTAssertEqual(item, logs[0])
        XCTAssertEqual(item, logs[1])
        XCTAssertEqual(item, logs[2])
        XCTAssertEqual(item, logs[3])
        XCTAssertEqual(item, logs[4])
    }
    
    func test_updateLogs() {
        var item = LogItem(level: LOGGER_LEVEL.DEBUG, user: "User", sender: "Sender", executeTime: 0, locationX: 0, locationY: 0, event: "Event", data: "Data")
        logger.addLog(item)
        logger.addLog(item)
        logger.addLog(item)
        logger.addLog(item)
        logger.addLog(item)
        
        item = LogItem(level: LOGGER_LEVEL.DEBUG, user: "User1", sender: "Sender2", executeTime: 0, locationX: 0, locationY: 0, event: "Event1", data: "Data1")
        logger.updateLog(NSDate(), index: 2, item: item)
        let logs = logger.readLogs(NSDate())
        XCTAssertNotEqual(item, logs[0])
        XCTAssertNotEqual(item, logs[1])
        XCTAssertEqual(item, logs[2])
        XCTAssertNotEqual(item, logs[3])
        XCTAssertNotEqual(item, logs[4])
    }
    
    func test_deleteLog() {
        let item = LogItem(level: LOGGER_LEVEL.DEBUG, user: "User", sender: "Sender", executeTime: 0, locationX: 0, locationY: 0, event: "Event", data: "Data")
        logger.addLog(item)
        logger.addLog(item)
        logger.addLog(item)
        logger.addLog(item)
        logger.addLog(item)
        logger.deleteLog(NSDate(), index: 2)
        let logs = logger.readLogs(NSDate())
        XCTAssertEqual(item, logs[0])
        XCTAssertEqual(item, logs[1])
        XCTAssertEqual(item, logs[2])
        XCTAssertEqual(item, logs[3])
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}

