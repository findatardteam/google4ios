//
//  LogItemTests.swift
//  Log
//
//  Created by Wu Tien Yuan on 2016/4/21.
//  Copyright © 2016年 Eskygo. All rights reserved.
//

import XCTest
@testable import FDLog

class LogItemTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func test_convert() {
        let item = LogItem(level: LOGGER_LEVEL.DEBUG, user: "User", sender: "Sender", executeTime: 0, locationX: 0, locationY: 0, event: "Event", data: "Data")
        do {
            let data: NSData = try logItemToJson(item)
            let string = NSString(data: data, encoding: NSUTF8StringEncoding)!
            let newItem: LogItem = try JsonToLogItem(string as String)
            XCTAssertEqual(newItem, item)
        }
        catch {
            print("\(error)")
            XCTAssert(false)
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}

